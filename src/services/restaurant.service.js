const { AppError } = require("../helpers/error");
const { Restaurant, User } = require("../models");

const getRestaurants = async () => {
  try {
    const restaurants = await Restaurant.findAll({
      //   include: "userLikes",
      include: [
        {
          association: "userLikes",
          attributes: {
            exclude: ["email", "password"],
          },
          through: {
            attributes: [],
          },
        },
        {
          association: "owner",
          attributes: {
            exclude: ["email", "password"],
          },
        },
      ],
    });
    return restaurants;
  } catch (error) {
    console.error(error);
    throw error;
  }
};

module.exports = {
  getRestaurants,
};
